
# include <Siv3D.hpp>
# include "include/Ball.hpp"

void Ball::init() {
	startTime = Time::MillisecSince1601();
	drawFlag = true;
	gameOver = false;
	pos = Point(Random(0, size.x), Random(0, size.y));
}

void Ball::update() {
	if (Circle(pos, r * ((double)time / (double)overTime)).leftClicked) drawFlag = false;
	uint64 now = Time::MillisecSince1601();
	time = now - startTime;
	if (time > overTime) gameOver = true;
}

void Ball::draw() const {
	if(drawFlag) Circle(pos, r * ((double)time / (double)overTime)).draw(ColorF(255.0 * ((double)time / (double)overTime), 0.0, 0.0, ((double)time / (double)overTime)));
}