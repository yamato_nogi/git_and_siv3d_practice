﻿
# include <Siv3D.hpp>
# include <HamFramework.hpp>
# include "include/CommonData.hpp"
# include "include/Title.hpp"
# include "include/Game.hpp"
# include "include/Result.hpp"

void Main()
{
	//Windowタイトルを指定
	Window::SetTitle(L"Practice!!!!!!");
	//Windowサイズを指定
	Window::Resize(1000, 600);

	MyApp manager;

	//Titleクラスをmanagerに追加(public MyApp::Scene を継承しているため可能)
	manager.add<Title>(L"Title");
	//Gameクラスをmanagerに追加
	manager.add<Game>(L"Game");
	//Resultクラスをmanagerに追加
	manager.add<Result>(L"Result");

	while (System::Update())
	{
		manager.updateAndDraw();
	}
}
