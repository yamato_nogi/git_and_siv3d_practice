# include <Siv3D.hpp>
# include <HamFramework.hpp>
# include "include/Ball.hpp"
# include "include/CommonData.hpp"
# include "include/Game.hpp"

void Game::init() {
	startTime = Time::MillisecSince1601();
	SummonTimeCount = Time::MillisecSince1601();
	if (!ball.empty()) ball.clear();
	ball.push_back(Ball());
}

void Game::update() {
	uint64 now = Time::MillisecSince1601();
	uint64 time = now - startTime;
	m_data->score = time;
	for (int i = 0; i < ball.size(); i++) {
		if (!ball[i].drawFlag) ball[i].init();
		ball[i].update();
		if (ball[i].gameOver) changeScene(L"Result");
	}
	if ((now - SummonTimeCount) > SummonTime) {
		SummonTimeCount = now;
		ball.push_back(Ball());
	}
}

void Game::draw() const {
	m_data->scoreFont(L"Score : ", m_data->score).draw();
	for (int i = 0; i < ball.size(); i++) ball[i].draw();
}