# pragma once
# include <Siv3D.hpp>
# include <HamFramework.hpp>
# include "Ball.hpp"
# include "CommonData.hpp"

class Game : public MyApp::Scene {
private:
	const uint64 SummonTime = 5 * 1000;
	Array<Ball> ball;
	uint64 startTime;
	uint64 SummonTimeCount;
public:
	void init() override;
	void update() override;
	void draw() const override;
};