# pragma once
# include <Siv3D.hpp>

class Ball {
private:
	const double r = 300.0;
	const uint64 overTime = 3 * 1000;
	Size size = Window::Size();
	Point pos;
	uint64 startTime;
public:
	uint64 time;
	bool drawFlag;
	bool gameOver;
	void init();
	void update();
	void draw() const;
};