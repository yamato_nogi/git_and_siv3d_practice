# pragma once
# include <Siv3D.hpp>
# include <HamFramework.hpp>

struct CommonData {
	uint64 score = 0;
	Font font{ 50 };
	Font scoreFont{ 30 };
};

using MyApp = SceneManager<String, CommonData>;