# include <Siv3D.hpp>
# include <HamFramework.hpp>
# include "include/CommonData.hpp"
# include "include/Title.hpp"

void Title::init() {
	m_data->score = 0;
}

void Title::update() {
	if (Input::MouseL.clicked) {
		changeScene(L"Game");
	}
}

void Title::draw() const {
	m_data->font(L"Title").drawCenter(Window::Center());
}